.. medicinde-doc documentation master file, created by
   sphinx-quickstart on Mon Feb 19 17:22:10 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to medicinde-doc's documentation!
=========================================

.. image:: _static/_images/main.png

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
